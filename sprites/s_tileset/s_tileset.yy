{
    "id": "68bfbef6-7522-4d79-a334-1c14dd753e60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "495c1795-c3bf-44e5-93b0-933414cab10f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68bfbef6-7522-4d79-a334-1c14dd753e60",
            "compositeImage": {
                "id": "06235527-ff83-4f3b-989a-c07f72ef5707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "495c1795-c3bf-44e5-93b0-933414cab10f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cccf490d-0e3a-4ced-8b19-a5c24414482a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "495c1795-c3bf-44e5-93b0-933414cab10f",
                    "LayerId": "dc8d96c9-ea6c-406b-801d-167324c0ad95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dc8d96c9-ea6c-406b-801d-167324c0ad95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68bfbef6-7522-4d79-a334-1c14dd753e60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}