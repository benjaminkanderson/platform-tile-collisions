{
    "id": "14a52873-3274-4146-9627-0114f79fb73e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8098c99b-0a01-4aff-bcd2-f144a9dc8853",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a52873-3274-4146-9627-0114f79fb73e",
            "compositeImage": {
                "id": "f7f61062-959f-4f52-b07d-5a10cd4aece8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8098c99b-0a01-4aff-bcd2-f144a9dc8853",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f629804-040c-4658-8f90-3df2ed5bc354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8098c99b-0a01-4aff-bcd2-f144a9dc8853",
                    "LayerId": "e0fa1cf7-3328-4f8e-b905-ed128f9f12bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e0fa1cf7-3328-4f8e-b905-ed128f9f12bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14a52873-3274-4146-9627-0114f79fb73e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}