///@param tile_map_id
///@param tile_size
///@param direction
///@param velocity
var tile_map_id = argument0;
var tile_size = argument1;
var facing = argument2;
var velocity = argument3;

// For the velocity array
var X = 0;
var Y = 1;

// Contact the tile based on the facing string
switch (facing) {
	case 0 :
		x = bbox_right&~(tile_size-1);
		x -= bbox_right-x;
		velocity[@ X] = 0;
		break;
		
	case 180 :
		x = bbox_left&~(tile_size-1);
		x += tile_size+x-bbox_left;
		velocity[@ X] = 0;
		break;
		
	case 270 :
		y = bbox_bottom&~(tile_size-1);
		y -= bbox_bottom-y;
		velocity[@ Y] = 0;
		break;
		
	case 90 :
		y = bbox_top&~(tile_size-1);
		y += tile_size+y-bbox_top;
		velocity[@ Y] = 0;
		break;
}