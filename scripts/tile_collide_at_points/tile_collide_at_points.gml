///@param tile_map_id
///@param point_arrays...
var tile_map_id	= argument[0];

// Extra variables
var found = 0;

// For the point arrays
var X = 0;
var Y = 1;

// Loop through the points and check for a tile
for (var i=1; i<=argument_count-1; i++) {
	var point = argument[i];
	found = found || tilemap_get_at_pixel(tile_map_id, point[X], point[Y]);
}

// Found will be the id of the tile or 0 if we didn't find one
return found;
