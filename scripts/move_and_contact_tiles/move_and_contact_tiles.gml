///@param tile_map_id
///@param tile_size
///@param velocity_array
var tile_map_id = argument0;
var tile_size = argument1;
var velocity = argument2;

// For the velocity array
var X = 0;
var Y = 1;

// Move horizontally
x += velocity[X];

// Right collisions
if velocity[X] > 0 {
	var tile_right = tile_collide_at_points(tile_map_id, [bbox_left, bbox_top], [bbox_right-1, bbox_bottom-1]);
	if tile_right {
		contact_tile(tile_map_id, tile_size, 0, velocity);
	}
} else {
	// Left collisions
	var tile_left = tile_collide_at_points(tile_map_id, [bbox_left, bbox_top], [bbox_left, bbox_bottom-1]);
	if tile_left {
		contact_tile(tile_map_id, tile_size, 180, velocity);
	}
}

// Move vertically
y += velocity[Y];

// Vertical collisions
if velocity[Y] > 0 {
	// Bottom collisions
	var tile_bottom = tile_collide_at_points(tile_map_id, [bbox_left, bbox_bottom-1], [bbox_right-1, bbox_bottom-1]);
	if tile_bottom {
		contact_tile(tile_map_id, tile_size, 270, velocity);
	}
} else {
	// Top collisions
	var tile_top = tile_collide_at_points(tile_map_id, [bbox_left, bbox_top], [bbox_right-1, bbox_top]);
	if tile_top {
		contact_tile(tile_map_id, tile_size, 90, velocity);
	}
}